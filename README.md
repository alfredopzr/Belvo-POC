# Belvo Quickstart


![Belvo quickstart app](/assets/quickstart-screenshot.png)


Please see our [dedicated Quickstart Guide](https://developers.belvo.com/docs/quickstart-application) for instructions on how to install and deploy the project.


**Notes regarding Belvo API POC**

This application requires the use of docker to run.
When in the quickstart folder, use the command 'make run' to start local deployment.

Had some console problems with CORS, when making API requests but that was solved by using a google extension.
Related link:
https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS/Errors/CORSMissingAllowOrigin

Relevant files are in 'client' and 'ruby' subfolders.

For more information about the sandbox or the quickstart provided by Belvo check these links:
https://developers.belvo.com/docs/test-in-sandbox
https://developers.belvo.com/docs/quickstart-application

Belvo Documentation:
https://docs.belvo.com/

